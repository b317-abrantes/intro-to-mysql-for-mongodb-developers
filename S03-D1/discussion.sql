-- [SECTION] Inserting Records

INSERT INTO artists (name) VALUES ("Rivermaya");
INSERT INTO artists (name) VALUES ("Psy");

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Psy 6", "2012-1-1", 2);
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Trip", "1996-1-1", 1);

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Gangnam Style", 253, "K-pop", 4);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Kundiman", 234, "OPM", 5);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Kisapmata", 279, "OPM", 5);

-- Notice that we're not including id in artists. That is because we set the id column to auto increment.

-- [SECTION] Selecting Records

-- Display the title and genre of all the songs.
SELECT title, genre FROM songs;

-- Display the title of all the OPM songs.
SELECT * FROM songs;

-- Display the title of all the OPM songs.
SELECT title FROM songs WHERE genre = "OPM";

-- We can use AND and OR keyword for multiple expressions in the WHERE clause.

-- Display the title and length of the OPM songs that are more than 4 minutes.
SELECT title, length FROM songs WHERE length > 240 AND genre = "OPM";

-- [SECTION] Updating Records

-- Update the length of Kundiman to 240 seconds.
UPDATE songs SET length = 240 WHERE title = "Kundiman";

-- Removing the WHERE clause will update all rows.

-- [SECTION] Deleting Records

-- Delete all OPM songs that are more than 4 minutes
DELETE FROM songs WHERE genre = "OPM" AND length > 240;

-- Removing the WHERE clause will delete all rows.

-- ALTER TABLE table_name MODIFY column_name datatype AUTO_INCREMENT = new_value;